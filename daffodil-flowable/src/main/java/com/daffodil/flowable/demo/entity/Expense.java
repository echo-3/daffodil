package com.daffodil.flowable.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 费用报销
 * 
 * @author yweijian
 * @date 2020年1月10日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "demo_expense")
public class Expense extends BaseEntity {

	private static final long serialVersionUID = 7865961110296726024L;

	/** 费用报销ID */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "expense_id")
	private String id;
	
	/** 费用报销主题 */
	@Column(name = "title")
	private String title;

	/** 费用报销金额 */
	@Column(name = "money")
	private Double money;

	/** 费用报销清单 */
	@Column(name = "content", length=4000)
	private String content;
	
	//为了方便对接流程只要在业务实体加上以下字段即可
	/** 流程类型ID */
	@Column(name="flow_type_id")
	private String flowTypeId;
	
	/** 流程部署实例ID */
	@Column(name="deploy_id")
	private String deployId;
	
	/** 流程模型标识 */
	@Column(name="model_key")
	private String modelKey;
	
	/** 创建者 */
	@Column(name="create_by")
	@Hql(type = Logical.LIKE)
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 更新者 */
	@Column(name="update_by")
	@Hql(type = Logical.LIKE)
	private String updateBy;

	/** 更新时间 */
	@Column(name="update_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	/** 备注 */
	@Column(name="remark")
	@Hql(type = Logical.LIKE)
	private String remark;
	
}
