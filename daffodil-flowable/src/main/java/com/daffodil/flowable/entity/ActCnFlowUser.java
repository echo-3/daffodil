package com.daffodil.flowable.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户绑定流程关系表
 * @author yweijian
 * @date 2020年2月11日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "act_cn_flow_user")
public class ActCnFlowUser extends BaseEntity{
	private static final long serialVersionUID = 3344315737025565993L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "model_user_id")
	private String id;
	
	/** 用户ID */
	@Column(name = "user_id")
	@Hql(type = Logical.EQ)
	private String userId;
	
	/** 流程类型ID */
	@Column(name = "flow_type_id")
	@Hql(type = Logical.EQ)
	private String flowTypeId;
	
	/** 部署ID（发布） */
	@Column(name = "deploy_id")
	@Hql(type = Logical.EQ)
	private String deployId;
	
	/** 流程标识 */
	@Column(name = "model_key")
	@Hql(type = Logical.EQ)
	private String modelKey;
	
	/** 状态 */
	@Dict("sys_data_status")
	@Hql(type = Logical.EQ)
	@Column(name = "status")
	private String status;
	
}
