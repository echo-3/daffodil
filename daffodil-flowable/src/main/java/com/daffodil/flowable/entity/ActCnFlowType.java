package com.daffodil.flowable.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.*;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 流程类型表
 * @author yweijian
 * @date 2020年2月11日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="act_cn_flow_type")
public class ActCnFlowType extends BaseEntity {
	private static final long serialVersionUID = -1825146557279002056L;

	/** 类型编号 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "flow_type_id")
	private String id;
	
	/** 父类型ID */
	@Column(name = "parent_id")
	private String parentId;

	/** 祖级列表 */
	@Column(name = "ancestors")
	private String ancestors;

	/** 类型名称 */
	@Column(name = "type_name")
	@NotBlank(message = "类型名称不能为空")
	@Size(min = 0, max = 30, message = "类型名称长度不能超过30个字符")
	@Hql(type = Logical.LIKE)
	private String typeName;
	
	/** 类型状态（0正常 1停用 2删除） */	
	@Column(name = "status")
	@Dict(value = "sys_data_status")
	@Hql(type = Logical.EQ)
	private String status;

	/** 显示顺序 */
	@Column(name = "order_num")
	private Long orderNum;

	/** 创建者 */
	@Column(name="create_by")
	@Hql(type = Logical.LIKE)
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 更新者 */
	@Column(name="update_by")
	@Hql(type = Logical.LIKE)
	private String updateBy;

	/** 更新时间 */
	@Column(name="update_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	/** 备注 */
	@Column(name="remark")
	@Hql(type = Logical.LIKE)
	private String remark;
	
}
