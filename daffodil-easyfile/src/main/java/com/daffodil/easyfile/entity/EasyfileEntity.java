package com.daffodil.easyfile.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 文件实体
 * @author yweijian
 * @date 2020年12月24日
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "easyfile_entity")
public class EasyfileEntity extends BaseEntity{

	private static final long serialVersionUID = -1088082494714166567L;
	
	/** 分片MD5唯一标识 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "file_id")
	private String id;
	
	/** 文件名称 */
	@Column(name= "file_name")
	@Hql(type = Logical.LIKE)
	private String fileName;
	
	/** 文件原名*/
	@Column(name= "original_name")
	@Hql(type = Logical.LIKE)
	private String originalName;
	
	/** 文件路径 */
	@Column(name= "file_path")
	@Hql(type = Logical.LIKE)
	private String filePath;
	
	/** 文件大小 */
	@Column(name= "file_size")
	private Long fileSize;
	
	/** 文件后缀*/
	@Column(name= "file_suffix")
	@Hql(type = Logical.EQ)
	private String fileSuffix;
	
	/** 文件类型 */
	@Column(name= "file_type")
	private String fileType;
	
	/** 文件状态（0正常 1停用 2删除） */
	@Column(name = "status")
	@Dict(value = "sys_data_status")
	@Hql(type = Logical.EQ)
	private String status;
	
	/** 创建者 */
	@Column(name="create_by")
	@Hql(type = Logical.LIKE)
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 更新者 */
	@Column(name="update_by")
	@Hql(type = Logical.LIKE)
	private String updateBy;

	/** 更新时间 */
	@Column(name="update_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	/** 备注 */
	@Column(name="remark")
	@Hql(type = Logical.LIKE)
	private String remark;

}
