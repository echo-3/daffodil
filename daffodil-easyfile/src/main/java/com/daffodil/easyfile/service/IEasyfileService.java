package com.daffodil.easyfile.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.daffodil.core.entity.Query;
import com.daffodil.easyfile.entity.EasyfileChunk;
import com.daffodil.easyfile.entity.EasyfileEntity;

/**
 * 
 * @author yweijian
 * @date 2020年12月24日
 * @version 1.0
 * @description
 */
public interface IEasyfileService {
	
	/**
	 * 分页查询文件信息
	 * @param query
	 * @return
	 */
	List<EasyfileEntity> selectEasyfileEntityList(Query<EasyfileEntity> query);
	/**
	 * 分片文件上传
	 * @param chunkFile		分片文件
	 * @param easyfileChunk	分片信息
	 * @param baseDir		上传物理路径
	 */
	public EasyfileChunk easyfileUpload(MultipartFile chunkFile, EasyfileChunk easyfileChunk, String baseDir);

	/**
	 * 分片文件合成
	 * @param easyfileChunk
	 * @param baseDir
	 * @return
	 */
	public EasyfileEntity easyfileMerge(EasyfileChunk easyfileChunk, String baseDir);
	
	/**
	 * 删除分片文件
	 * @param easyfileChunk
	 */
	public void removeEasyfileChunk(EasyfileChunk easyfileChunk);
	
	/**
	 * 根据ID查询文件信息
	 * @param id
	 * @return
	 */
	public EasyfileEntity selectEasyfileEntityById(String id);
	
	/**
	 * 删除文件
	 * @param ids
	 */
	public void deleteEasyfileEntityByIds(String[] ids);
	
}
