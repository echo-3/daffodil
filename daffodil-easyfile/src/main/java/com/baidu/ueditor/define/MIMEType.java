package com.baidu.ueditor.define;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @date 2021年2月7日
 * @version 1.0
 * @description
 */
public class MIMEType {

	public static final Map<String, String> types = new HashMap<String, String>() {
		private static final long serialVersionUID = 1L;

		{
			put("image/gif", ".gif");
			put("image/jpeg", ".jpg");
			put("image/jpg", ".jpg");
			put("image/png", ".png");
			put("image/bmp", ".bmp");
		}
	};

	public static String getSuffix(String mime) {
		return MIMEType.types.get(mime);
	}

}
