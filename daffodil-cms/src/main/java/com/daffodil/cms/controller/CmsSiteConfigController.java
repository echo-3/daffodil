package com.daffodil.cms.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daffodil.cms.entity.CmsChannel;
import com.daffodil.cms.entity.CmsSiteConfig;
import com.daffodil.cms.service.ICmsChannelService;
import com.daffodil.cms.service.ICmsSiteConfigService;
import com.daffodil.core.annotation.FormToken;
import com.daffodil.core.annotation.FormToken.TokenType;
import com.daffodil.core.annotation.Log;
import com.daffodil.core.annotation.Log.BusinessType;
import com.daffodil.core.controller.BaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.util.StringUtils;

/**
 * 站点配置 信息控制层
 * @author yweijian
 * @date 2020年10月29日
 * @version 1.0
 * @description
 */
@Controller
@RequestMapping("/cms/site/config")
public class CmsSiteConfigController extends BaseController {
	
	private String prefix = "cms/site/config";

	@Autowired
	private ICmsSiteConfigService configService;

	@Autowired
	private ICmsChannelService channelService;
	
	/**
	 * 新增/修改站点配置
	 */
	@FormToken
	@GetMapping("/edit/{siteId}")
	public String edit(@PathVariable("siteId") String siteId, ModelMap modelMap) {
		CmsSiteConfig config = configService.selectSiteConfigBySiteId(siteId);
		if(StringUtils.isNull(config)) {
			config = new CmsSiteConfig();
		}
		CmsChannel site = channelService.selectChannelById(siteId);
		config.setSite(site);
		modelMap.put("config", config);
		return prefix + "/edit";
	}

	/**
	 * 保存站点配置
	 */
	@FormToken(TokenType.DESTROY)
	@RequiresPermissions("cms:site_config:edit")
	@Log(title = "站点配置", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public JsonResult editSave(CmsSiteConfig config) {
		if(StringUtils.isNotEmpty(config.getId())) {
			configService.updateSiteConfig(config);
		}else {
			configService.insertSiteConfig(config);
		}
		return JsonResult.success();
	}

}
