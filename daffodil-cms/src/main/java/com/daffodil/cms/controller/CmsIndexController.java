package com.daffodil.cms.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.daffodil.system.entity.SysUser;
import com.daffodil.core.controller.BaseController;
import com.daffodil.framework.shiro.util.ShiroUtils;

/**
 * CMS首页控制层
 * @author yweijian
 * @date 2020年10月26日
 * @version 1.0
 * @description
 */
@Controller
@RequestMapping("/cms")
public class CmsIndexController extends BaseController {

	@GetMapping("/index")
	@RequiresPermissions("cms:index:view")
	public String index(ModelMap modelMap) {
		SysUser user = ShiroUtils.getSysUser();
		modelMap.put("user", user);
		return "cms/index";
	}
	
}
