package com.daffodil.cms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * （稿件）文档--历史数据表
 * @author yweijian
 * @date 2020年10月25日
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="cms_document_his")
public class CmsDocumentHis extends BaseEntity {
	private static final long serialVersionUID = -6825959925082228308L;

	/** 历史稿件编号 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "id")
	private String id;
	
	/** 原始稿件ID */
	@Column(name = "doc_id")
	private String documentId;
	
	/** 站点ID */
	@Column(name = "site_id")
	@Hql(type = Logical.EQ)
	private String siteId;
	
	/** 栏目ID */
	@Column(name = "channel_id")
	@Hql(type = Logical.EQ)
	private String channelId;
	
	/** 栏目名称（不作为字段） */
	@Transient
	private String channelName;
	
	/** 稿件类型 （0图文稿 1音频稿 2视频稿）*/
	@Column(name = "doc_type")
	@Dict(value = "cms_document_type")
	@Hql(type = Logical.EQ)
	private String docType;
	
	/** 稿件标题 */
	@Column(name = "title")
	@NotBlank(message = "标题不能为空")
	@Size(min = 0, max = 100, message = "标题长度不能超过100个字符")
	@Hql(type = Logical.LIKE)
	private String title;
	
	/** 稿件副标题 */
	@Column(name = "subtitle")
	@Size(min = 0, max = 100, message = "副标题长度不能超过100个字符")
	@Hql(type = Logical.LIKE)
	private String subtitle;
	
	/** 稿件正文 */
	@Column(name = "content", columnDefinition="longtext")
	private String content;
	
	/** 稿件正文--带html标签的正文 */
	@Column(name = "html_content", columnDefinition="longtext")
	private String htmlContent;
	
	/** 稿件正文字数 */
	@Column(name = "words_count")
	private Integer wordsCount;
	
	/** 稿件摘要 */
	@Column(name = "doc_abstract", length = 512)
	@Size(min = 0, max = 512, message = "摘要长度不能超过512个字符")
	@Hql(type = Logical.LIKE)
	private String docAbstract;
	
	/** 稿件关键字 */
	@Column(name = "doc_keywords", length = 512)
	@Size(min = 0, max = 512, message = "关键字长度不能超过512个字符")
	@Hql(type = Logical.LIKE)
	private String docKeywords;
	
	/** 稿件作者 */
	@Column(name = "doc_author")
	@Hql(type = Logical.LIKE)
	private String docAuthor;
	
	/** 稿件撰写时间 */
	@Column(name = "write_time")
	private Date writeTime;
	
	/** 原文链接地址 */
	@Column(name = "original_http_url")
	private String originalHttpUrl;
	
	/** 发布链接地址 */
	@Column(name = "release_http_url")
	private String releaseHttpUrl;
	
	/** 稿件状态 （0草稿 1待审 2已审 3待发布 4已发布 5已下架）*/
	@Column(name = "doc_status")
	@Dict(value = "cms_document_status")
	@Hql(type = Logical.EQ)
	private String docStatus;

	/** 显示顺序 */
	@Column(name = "order_num")
	private Long orderNum;

	/** 稿件状态（0正常 1停用 2删除） */
	@Column(name = "status")
	@Dict(value = "sys_data_status")
	@Hql(type = Logical.EQ)
	private String status;
	
	/** 创建者 */
	@Column(name="create_by")
	@Hql(type = Logical.LIKE)
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 更新者 */
	@Column(name="update_by")
	@Hql(type = Logical.LIKE)
	private String updateBy;

	/** 更新时间 */
	@Column(name="update_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	/** 备注 */
	@Column(name="remark")
	@Hql(type = Logical.LIKE)
	private String remark;
}
