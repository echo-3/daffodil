package com.daffodil.core.entity;

/**
 * Ztree树结构实体类
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
public class Ztree extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	/** 节点父ID */
	private String pId;

	/** 节点名称 */
	private String name;

	/** 节点标题 */
	private String title;

	/** 是否勾选 */
	private boolean checked = false;

	/** 是否展开 */
	private boolean open = false;

	/** 是否能勾选 */
	private boolean nocheck = false;
	
	public String getpId() {
		return pId;
	}

	public void setpId(String pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public boolean isNocheck() {
		return nocheck;
	}

	public void setNocheck(boolean nocheck) {
		this.nocheck = nocheck;
	}
}
