package com.daffodil.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义hql语句创建注解
 * @author yweijian
 * @date 2020年2月14日
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Documented
public @interface Hql {
	
	/**
	 * 逻辑运算类型
	 * LIKE("like"), EQ("="), NEQ("!="), LEQ("<="), LT("<"), REQ(">="), RT(">"), IN("in"), NONE("");
	 * @return
	 */
	Logical type() default Logical.NONE;
	
	public enum Logical {
		/**
		 * 模糊匹配 col like '%char%'
		 */
		LIKE("like"), 
		/**
		 * 相等匹配 col = 'char'
		 */
		EQ("="), 
		/**
		 * 不等匹配 col != 'char'
		 */
		NEQ("!="), 
		/**
		 * 小于等于匹配 col <= 123
		 */
		LEQ("<="), 
		/**
		 * 小于匹配 col < 123
		 */
		LT("<"), 
		/**
		 * 大于等于匹配 col >= 123
		 */
		REQ(">="), 
		/**
		 * 大于匹配 col > 123
		 */
		RT(">"), 
		/**
		 * 包含匹配 col in('char1','char2')
		 */
		IN("in"), 
		/**
		 * 空字符（无匹配意义）
		 */
		NONE("");
		private final String value;

		Logical(String value) {
			this.value = value;
		}

		public String value() {
			return this.value;
		}
	}

}