package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统过滤约束配置
 * @author yweijian
 * @date 2020年2月15日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_shiro_filter")
public class SysShiroFilter extends BaseEntity{
	private static final long serialVersionUID = 2584359182626267762L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "filter_id")
	private String id;
	
	/** 过滤约束名称 */
	@Column(name = "filter_name")
	@Hql(type = Logical.LIKE)
	private String filterName;
	
	/** 过滤约束表达式 */
	@Column(name = "filter_key")
	@Hql(type = Logical.LIKE)
	private String filterKey;
	
	/** 过滤约束方法 */
	@Column(name = "filter_value")
	@Hql(type = Logical.LIKE)
	private String filterValue;
	
	/** 状态 */
	@Column(name = "status")
	@Dict(value = "sys_data_status")
	@Hql(type = Logical.EQ)
	private String status;
	
	/** 显示顺序 */
	@Column(name = "order_num")
	private Long orderNum;
	
	/** 创建者 */
	@Column(name="create_by")
	@Hql(type = Logical.LIKE)
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 更新者 */
	@Column(name="update_by")
	@Hql(type = Logical.LIKE)
	private String updateBy;

	/** 更新时间 */
	@Column(name="update_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	/** 备注 */
	@Column(name="remark")
	@Hql(type = Logical.LIKE)
	private String remark;
	
}
