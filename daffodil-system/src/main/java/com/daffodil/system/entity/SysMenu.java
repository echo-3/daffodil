package com.daffodil.system.entity;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.*;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.Ztree;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 菜单权限表
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_menu")
public class SysMenu extends Ztree {
	private static final long serialVersionUID = 1642572849663654460L;

	/** 菜单编号 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "menu_id")
	private String id;
	
	/** 菜单名称 */
	@Column(name = "menu_name")
	@NotBlank(message = "菜单名称不能为空")
	@Size(min = 0, max = 50, message = "菜单名称长度不能超过50个字符")
	@Hql(type = Logical.LIKE)
	private String menuName;

	/** 父菜单ID */
	@Column(name = "parent_id")
	@Hql(type = Logical.EQ)
	private String parentId;

	/** 祖级列表 */
	@Column(name = "ancestors")
	private String ancestors;

	/** 显示顺序 */
	@Column(name = "order_num")
	private Long orderNum;

	/** 菜单URL */
	@Column(name = "url")
	@Size(min = 0, max = 200, message = "请求地址不能超过200个字符")
	@Hql(type = Logical.LIKE)
	private String url;

	/** 打开方式：menuItem页签 menuBlank新窗口 */
	@Column(name = "target")
	@Hql(type = Logical.EQ)
	private String target;

	/** 类型:0目录,1菜单,2按钮 */
	@Column(name = "menu_type")
	@Dict(value = "sys_menu_type")
	@NotBlank(message = "菜单类型不能为空")
	@Hql(type = Logical.EQ)
	private String menuType;

	/** 菜单状态:0显示,1隐藏 */
	@Column(name = "visible")
	@Dict(value = "sys_show_hide")
	@Hql(type = Logical.EQ)
	private String visible;

	/** 权限字符串 */
	@Column(name = "perms")
	@Size(min = 0, max = 100, message = "权限标识长度不能超过100个字符")
	@Hql(type = Logical.LIKE)
	private String perms;

	/** 菜单图标 */
	@Column(name = "icon")
	private String icon;

	/** 创建者 */
	@Column(name="create_by")
	@Hql(type = Logical.LIKE)
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 更新者 */
	@Column(name="update_by")
	@Hql(type = Logical.LIKE)
	private String updateBy;

	/** 更新时间 */
	@Column(name="update_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	/** 备注 */
	@Column(name="remark")
	@Hql(type = Logical.LIKE)
	private String remark;

	/** 子菜单 */
	@Transient
	private List<SysMenu> children = new ArrayList<SysMenu>();

}
