package com.daffodil.framework.shiro.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.apache.shiro.session.SessionException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.daffodil.core.constant.Constants;
import com.daffodil.framework.shiro.service.SysShiroService;
import com.daffodil.framework.shiro.util.ShiroUtils;
import com.daffodil.system.entity.SysUser;
import com.daffodil.util.MessageUtils;
import com.daffodil.util.StringUtils;
import com.daffodil.util.spring.SpringUtils;

/**
 * 退出过滤器
 * @author yweijian
 * @date 2020年12月18日
 * @version 1.0
 * @description
 */
public class SysLogoutFilter extends LogoutFilter {
	
	private static final Logger log = LoggerFactory.getLogger(SysLogoutFilter.class);

	/**
	 * 退出后重定向的地址
	 */
	private String loginUrl;
	
	public SysLogoutFilter(String loginUrl) {
		super();
		this.loginUrl = loginUrl;
	}

	@Override
	protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
		Subject subject = super.getSubject(request, response);
		String redirectUrl = this.getRedirectUrl(request, response, subject);
		SysShiroService shiroService = SpringUtils.getBean(SysShiroService.class);
		try {
			// 更新用户在线日志
			shiroService.deleteOnlineSession(ShiroUtils.getSessionId());
			// 记录用户退出日志
			SysUser user = ShiroUtils.getSysUser();
			if(user != null) {
				shiroService.recordLoginInfo(user.getLoginName(), Constants.LOGOUT, MessageUtils.message("user.logout.success"));
			}
			// 清理登录缓存
			ShiroUtils.clearLoginCache();
			// 退出登录
			subject.logout();
		} catch (SessionException e) {
			log.error("用户注销失败", e);
		}
		super.issueRedirect(request, response, redirectUrl);
		return false;
	}

	/**
	 * 退出跳转URL
	 */
	@Override
	protected String getRedirectUrl(ServletRequest request, ServletResponse response, Subject subject) {
		if (StringUtils.isNotEmpty(loginUrl)) {
			return loginUrl;
		}
		return super.getRedirectUrl(request, response, subject);
	}

}
