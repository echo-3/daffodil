# Daffodil（达佛）基础管理平台`免费开源` <a href='https://gitee.com/weijiang_admin/daffodil/stargazers'><img src='https://gitee.com/weijiang_admin/daffodil/badge/star.svg?theme=dark' alt='star'></img></a>
一个简单、快速、易读的maven项目

本人出生地水仙花之都【[漳州](http://www.zhangzhou.gov.cn/)】

Daffodil英文翻译意思是水仙花，本人也比较佛系且Daffodil音含 [ 达佛 ] 于是就叫“达佛基础管理平台”

#### 演示地址
[达佛基础管理平台http://www.daffodil.vip/login](http://www.daffodil.vip/login)

#### 介绍
本系统是基于SpringBoot的后台管理系统 易读易懂、界面简洁美观。 采用技术SpringBoot、SpingDataJpa、Redis、Shiro、Flowable、thymeleaf、Redis等。

系统基础包括：用户管理、角色管理、部门管理、岗位管理、权限管理、菜单管理、字典管理、约束管理、参数配置、通知公告、日志管理、流程管理、任务管理、流程演示、系统监控、代码生成等模块...

主要模块：
- daffodil-code：底层核心代码库
- daffodil-system：基础后台
- daffodil-flowable：流程引擎（实现流程引擎交互功能）
- faffodil-easyfile：文件上传（实现大文件分片上传、续传、秒传功能）
- daffodil-ui：系统前端文件（使用thymeleaf模板引擎技术实现）
- daffodil-starter：系统集成启动入口（当然单独模块启动也可以~~）

其它模块：
- daffodil-jasypt：配置文件数据加密工具（使用java的GUI实现）
- daffodil-devtool：代码生成（代码生成器其实实际开发使用不怎么用~~）
- daffodil-cms：内容管理（闲暇慢慢实现）

#### 软件架构
软件架构说明
- 核心框架：Spring Boot
- 安全框架：Apache Shiro
- 持久层框架：SpingDataJpa
- 工作流引擎：flowable
- 模板引擎：Thymeleaf
- 缓存中间件：Redis
- 前端技术：bootstrap、layui、fontawesome等

#### 开发教程

以myeclipse开发工具为例：
1.  将项目检出并导入到你的工作空间。
2.  修改daffodil-starter启动项目的数据库资源配置文件application-datasource.properties的数据源配置。
3.  启动Redis缓存中间件，默认端口6379。
4.  运行DaffodilApplication，如果不出意外正常启动成功。
5.  访问系统[http://127.0.0.1:8080/] 超级管理员账号：admin 密码：123456，点击登录愉快的玩耍吧！

--备注：
1.  第一次启动成功后，请注释掉daffodil-starter启动项目DaffodilApplication类的main()方法中的init()初始化方法。
2.  可以修改daffodil-flowable项目的application-flowable.properties的数据库更新策略database-schema-update=true（仅第一次运行的时候），后续为了提高性能可设置为false。
3.  目前系统只准备了Mysql的初始化数据。如果你想用SqlServer或者Oracle，那么你可以自行手动将Mysql的数据库基础数据导入到SqlServer或者Oracle数据库中，接下来请修改配置文件的数据库驱动driver-class和数据库方言database-platform。


#### 系统效果
![输入图片说明](https://images.gitee.com/uploads/images/2020/0711/164432_9f3607e5_332368.png "QQ截图20200711163001.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0711/164441_86c51cea_332368.png "QQ截图20200711163028.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/163122_44a44933_332368.png "QQ截图20200722163038.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0711/164449_77dbb0af_332368.png "QQ截图20200711163045.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0711/164457_2080e84f_332368.png "QQ截图20200711163132.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0711/164506_b217cc5e_332368.png "QQ截图20200711163213.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0711/164521_394197fa_332368.png "QQ截图20200711163205.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
